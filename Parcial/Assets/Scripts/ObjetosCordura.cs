using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjetosCordura : MonoBehaviour
{
  
    private bool control;
    void Start()
    {
        control = true;
    }

    
    void Update()
    {
        
        
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            StartCoroutine(DescensoCordura());
        }
    }
    IEnumerator DescensoCordura()
    {
        if (control==true)
        {
            control = false;
            yield return new WaitForSeconds(0.5f);
            GameManager.instance.jugador.ModificarCordura(-5);
            control = true;
        }
    }
}
