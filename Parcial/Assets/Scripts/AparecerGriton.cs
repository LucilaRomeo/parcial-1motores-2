using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AparecerGriton : MonoBehaviour
{
    public GameObject EnemigoGriton;
    public Transform posicionPivot;
    public bool aparecio=true;
    public int ControlEnemigo;
    public float timerspawn;

    void Start()
    {
        
        EnemigoGriton.SetActive(false);
    }

  
    void Update()
    {
        if (timerspawn <= 0 && GameManager.instance.jugador.Cordura<=95)
        {
            StartCoroutine(AparecerEnemigo());
            timerspawn = Random.Range(40f, 80f);

        }
        else if(!aparecio && timerspawn>0)
        {
            timerspawn -= Time.deltaTime;
        }
       
    }

    IEnumerator AparecerEnemigo()
    {
            aparecio = true;
            EnemigoGriton.transform.position = posicionPivot.position;
            EnemigoGriton.transform.rotation = posicionPivot.rotation;

            EnemigoGriton.SetActive(true);
            GestorDeAudio.instancia.ReproducirSonido("GritoEnemigo");
            GameManager.instance.jugador.ModificarRitmo(5);

            yield return new WaitForSeconds(5f);
            
            EnemigoGriton.SetActive(false);
            aparecio=false;
    }                 
}
