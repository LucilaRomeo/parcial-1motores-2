using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoFinal : MonoBehaviour
{
    public GameObject perdiste;
    public GameObject ganaste;
    public Animator animator;

    private void Start()
    {
        perdiste.SetActive(false);
        ganaste.SetActive(false);
    }
    void Update()
    {
       
    }

    private void OnTriggerEnter(Collider other)
    {
        if(GameManager.instance.jugador.Cordura<=15 && other.gameObject.CompareTag("Player"))
        {
            perdiste.SetActive(true);
            GestorDeAudio.instancia.ReproducirSonido("EnemigoFinal");
            animacion();
        }
        else if(GameManager.instance.jugador.Cordura>=16&& other.gameObject.CompareTag("Player"))
        {
            ganaste.SetActive(true);
            GestorDeAudio.instancia.ReproducirSonido("EnemigoFinal");
            animacion();
        }
    }

    public void animacion()
    {
        animator.Play("Panel");
    }
}
