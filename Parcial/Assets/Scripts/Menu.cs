using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
    public GameObject ImgLetras;
    public GameObject Imgmenu;
    public Animator animator;
    void Start()
    {
        
    }

  
    void Update()
    {
        
    }
    public void jugar()
    {
        Imgmenu.SetActive(false);
        ImgLetras.SetActive(false);
        
        Cursor.lockState = CursorLockMode.Locked;
    }
    public void letras()
    {
        Imgmenu.SetActive(false);
        ImgLetras.SetActive(true);
    }
    public void atras()
    {
        Imgmenu.SetActive(true);
        ImgLetras.SetActive(false);
    }
}
