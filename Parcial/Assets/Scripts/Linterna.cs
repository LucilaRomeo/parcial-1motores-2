using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Linterna : MonoBehaviour
{
    public Light luzLinterna;
    public bool encendido;
    void Start()
    {
        
    }

    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            GestorDeAudio.instancia.ReproducirSonido("Linterna");
            encendido = !encendido;
            if (encendido == true)
            {
                luzLinterna.enabled = true;

            }
            if(encendido == false)
            {
                luzLinterna.enabled = false;
            }
        }
    }
}
