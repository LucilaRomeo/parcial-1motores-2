using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Llave : MonoBehaviour
{
    public GameObject llave;
    public GameObject llaveEnMano;
    public static bool TieneLlave;
    public GameObject tocaE;
    void Start()
    {
        llave.SetActive(true);
        llaveEnMano.SetActive(false);
        TieneLlave = false;
        tocaE.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            tocaE.SetActive(true);

        }
    }
    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.CompareTag("Player")&& Input.GetKeyDown(KeyCode.E))
        {
            GestorDeAudio.instancia.ReproducirSonido("TomarObjeto");
            llave.SetActive(false);
            TieneLlave=true;
            llaveEnMano.SetActive(true);
        }
    }
  
}
