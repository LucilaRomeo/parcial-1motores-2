using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{

    private Rigidbody rb;
    public float rapidez;
    public Vector2 sensibilidad;
    private Transform camara;
    public float Cordura=100;
    public float ritmoCardiaco;
    private bool control;
    public GameObject lluvia;
    public int pastillas;



    void Start()
    {
        GameManager.instance.jugador = this;
        rb = GetComponent<Rigidbody>();
        camara = transform.Find("Main Camera");
       
        lluvia.SetActive(false);
    }

    void Update()
    {

        movimiento();
        Lluvia();

    }
    public void movimiento()
    {

        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");

        transform.position += transform.forward * rapidez * movimientoVertical * Time.deltaTime;
        transform.position += transform.right * rapidez * movimientoHorizontal * Time.deltaTime;

        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");
        if (mouseX != 0)
        {
            transform.Rotate(Vector3.up * mouseX * sensibilidad.x);

        }
        if (mouseY != 0)
        {
            float angle = (camara.localEulerAngles.x - mouseY * sensibilidad.y + 360) % 360;
            if (angle > 180)
            {
                angle -= 360;
            }
            angle = Mathf.Clamp(angle, -80, 80);
            camara.localEulerAngles = Vector3.right * angle;
        }
    }
    public void ModificarCordura(int valor)
    {
        Cordura += valor;
        Debug.Log(Cordura);
    }

    public void ModificarRitmo(int valor)
    {
        ritmoCardiaco += valor;
        if(ritmoCardiaco < 0)
        {
            ritmoCardiaco = 0;
        }
        Debug.Log(ritmoCardiaco);
    }
    public void RitmoCardiacoSonido()
    {
        if(ritmoCardiaco>= 80)
        {
            GestorDeAudio.instancia.ReproducirSonido("RitmoCardiaco");
        }
    }

    public void CorduraAudio()
    {
        if (Cordura <= 20)
        {
            GestorDeAudio.instancia.ReproducirSonido("CorduraVoces");
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("EnemigoLinterna"))
        {
            EnemigoLinterna();
        }
    }
    IEnumerator EnemigoLinterna()
    {
        if (control)
        {
            control = false;
            yield return new WaitForSeconds(0.5f);
            Cordura--;
            ritmoCardiaco++;
            control = true;
        }
    }
    public void Lluvia()
    {
        if(Cordura <= 30)
        {
            lluvia.SetActive(true);
        }
    }
}
