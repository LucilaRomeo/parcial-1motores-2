using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCiglesia : MonoBehaviour
{
    public GameObject NPCiglesiaUno;
    public GameObject NPCiglesiaDos;
    public GameObject NPCiglesiaTres;
    public GameObject NPCiglesiaCuatro;
    public GameObject NPCiglesiaCinco;
    public GameObject TocaE;
    public GameObject Instrucciones2;
    public GameObject Instrucciones3;
    private bool skip;

    void Start()
    {
        NPCiglesiaUno.SetActive(false);
        NPCiglesiaDos.SetActive(false);
        NPCiglesiaTres.SetActive(false);
        NPCiglesiaCuatro.SetActive(false);
        NPCiglesiaCinco.SetActive(false);
        Instrucciones2.SetActive(false);
        Instrucciones3.SetActive(false);
        TocaE.SetActive(false);
    }

   
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            TocaE.SetActive(true);

        }
    }
    private void OnTriggerStay(Collider other)
    {
 
        if(other.gameObject.CompareTag("Player")&& Input.GetKeyDown(KeyCode.E))
        {
            TocaE.SetActive(false);
            GestorDeAudio.instancia.ReproducirSonido("NPC");
            StartCoroutine(Texto2());

        }
    }
    
    IEnumerator Texto2()
    {
        yield return new WaitForSeconds(0.5f);
        Instrucciones2.SetActive(true);
        NPCiglesiaDos.SetActive(false);
        NPCiglesiaUno.SetActive(true);
        TocaE.SetActive(false);

        if (skip == false)
        {
            StartCoroutine(Texto3());
        }
    }
    IEnumerator Texto3()
    {
        yield return new WaitForSeconds(5f);
        Instrucciones2.SetActive(false);
        NPCiglesiaUno.SetActive(false);
        NPCiglesiaTres.SetActive(true);
        if (skip == false)
        {
            StartCoroutine(Texto4());
        }
    }
    IEnumerator Texto4()
    {
        yield return new WaitForSeconds(5f);
        Instrucciones3.SetActive(true);
        NPCiglesiaTres.SetActive(false);
        NPCiglesiaCuatro.SetActive(true);
        if (skip == false)
        {
            StartCoroutine(Texto5());
        }
    }
    IEnumerator Texto5()
    {
        yield return new WaitForSeconds(5f);
        Instrucciones3.SetActive(false);
        NPCiglesiaCuatro.SetActive(false);
        NPCiglesiaCinco.SetActive(true);
        StartCoroutine(Finalizar());
       
    }

    IEnumerator Finalizar()
    {
        GestorDeAudio.instancia.PausarSonido("NPC");
        yield return new WaitForSeconds(5f);

      NPCiglesiaCinco.SetActive(false);

    }

}
