using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fusible : MonoBehaviour
{
    public GameObject fusible;
    public GameObject fusibleEnMano;
    public static bool JugadorTieneFusible;
    public GameObject tocaE;
    void Start()
    {
        fusible.SetActive(true);
        fusibleEnMano.SetActive(false);
        JugadorTieneFusible = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            tocaE.SetActive(true);

        }
    }
    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.CompareTag("Player")&& Input.GetKeyDown(KeyCode.E))
        {
            GestorDeAudio.instancia.ReproducirSonido("TomarObjeto");
            fusible.SetActive(false);
            fusibleEnMano.SetActive(true);
            JugadorTieneFusible = true;
        }
    }
}
