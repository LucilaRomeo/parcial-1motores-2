using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoCordura : MonoBehaviour
{
    public GameObject enemigo;
    public bool aparecio;
    private bool control;
    void Start()
    { 
       enemigo.SetActive(false);
        control = true;
    }


    void Update()
    {
      
        aparecer();
    }

    public void aparecer()
    {
        if (GameManager.instance.jugador.Cordura <= 50)
        {  
            enemigo.SetActive(true);
            GameManager.instance.jugador.ModificarRitmo(10);
            GameManager.instance.jugador.ModificarCordura(-5);
            GestorDeAudio.instancia.ReproducirSonido("EnemigoCordura");
        }
    }

   
}
