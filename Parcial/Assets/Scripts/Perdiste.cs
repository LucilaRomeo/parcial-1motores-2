using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perdiste : MonoBehaviour
{
   public Animator animator;
    void Start()
    {
        
    }   
    void Update()
    {
        if (GameManager.instance.jugador.Cordura <= 0)
        {
            animacion();

        }
        else if (GameManager.instance.jugador.ritmoCardiaco >= 100)
        {
            animacion();
        }
    }

    public void animacion()
    {
        animator.Play("Panel");
    }
}
