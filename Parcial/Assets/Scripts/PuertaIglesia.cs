using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuertaIglesia : MonoBehaviour
{
    public GameObject puertaCerrada;
    public GameObject puertaAbierta;
    public GameObject tocaE;
    void Start()
    {
        tocaE.SetActive(false);
        puertaAbierta.SetActive(false);
        puertaCerrada.SetActive(true);
    
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            tocaE.SetActive(true);

        }
    }
    private void OnTriggerStay(Collider other)
    {
        if(Llave.TieneLlave==true && other.gameObject.CompareTag("Player"))
        {
            tocaE.SetActive(false);
            puertaAbierta.SetActive(true);
            puertaCerrada.SetActive(false);
        }
    }
}
