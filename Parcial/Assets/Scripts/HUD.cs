using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class HUD : MonoBehaviour
{
    public Text RitmoCardiacoText;
    public Text CorduraText;
    public Text PastillasText;
    
   
    void Start()
    {
        
    }

    
    void Update()
    {
        RitmoCardiacoText.text = GameManager.instance.jugador.ritmoCardiaco.ToString();
        CorduraText.text=GameManager.instance.jugador.Cordura.ToString();
        PastillasText.text=GameManager.instance.jugador.pastillas.ToString();
        
        

    }
}
