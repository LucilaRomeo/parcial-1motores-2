using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Colisiones : MonoBehaviour
{
    public Text pastillasText;
    public int CantHPills=0;
    

    void Start()
    {
       
    }

    void Update()
    {
        TomarPastillasCardiacas();
        pastillasText.text= CantHPills.ToString();
        
    }

    public void OnTriggerStay(Collider other)
    {

        if (other.gameObject.CompareTag("HeartPills") && Input.GetKeyDown(KeyCode.E))
        {
            GestorDeAudio.instancia.ReproducirSonido("TomarObjeto");
            other.gameObject.SetActive(false);
            CantHPills+=2;
            GameManager.instance.jugador.pastillas = CantHPills;
        }  
    }

    public void TomarPastillasCardiacas()
    {
        if (CantHPills > 0 && Input.GetKeyDown(KeyCode.T))
        {
            GameManager.instance.jugador.ModificarRitmo(-20);
            GestorDeAudio.instancia.ReproducirSonido("TomarPastillas");
            CantHPills--;
            GameManager.instance.jugador.pastillas = CantHPills;
        }
    }
    
}
