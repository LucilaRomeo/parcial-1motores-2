using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCmercader : MonoBehaviour
{
    public GameObject NPCmercaderUno;
    public GameObject NPCmercaderDos;
    public GameObject NPCmercaderTres;
    public GameObject NPCmercaderCuatro;
    public GameObject NPCmercaderCinco;
    public GameObject NPCmercaderSeis;
    public GameObject NPCmercaderSiete;
    public GameObject LinternaEnMano;
    public GameObject Instrucciones1;
    public GameObject Linterna;
    public GameObject tocaE;
    public bool skip = false;
    public bool TieneFusible;
    void Start()
    {
        NPCmercaderUno.SetActive(false);
        NPCmercaderDos.SetActive(false);
        NPCmercaderTres.SetActive(false);
        NPCmercaderCuatro.SetActive(false);
        NPCmercaderCinco.SetActive(false);
        NPCmercaderSeis.SetActive(false);
        NPCmercaderSiete.SetActive(false);
        LinternaEnMano.SetActive(false);
        Instrucciones1.SetActive(false);
        tocaE.SetActive(false);
        Linterna.SetActive(true);
    }

   
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            tocaE.SetActive(true);

        }
    }
    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.CompareTag("Player")&& Input.GetKeyDown(KeyCode.E))
        {
            tocaE.SetActive(false);
            GestorDeAudio.instancia.ReproducirSonido("NPC");
            StartCoroutine(Texto2());
        }
        
    }
    IEnumerator Texto2()
    {
        yield return new WaitForSeconds(0.5f);
        NPCmercaderDos.SetActive(false);
        NPCmercaderUno.SetActive(true);
        tocaE.SetActive(false);
        if (skip == false)
        {
            StartCoroutine(Texto3());
        }
    }
    IEnumerator Texto3()
    {
        yield return new WaitForSeconds(3f);
        NPCmercaderUno.SetActive(false);
        NPCmercaderTres.SetActive(true);
        if (skip == false)
        {
            StartCoroutine(Texto4());
        }
    }
    IEnumerator Texto4()
    {
        yield return new WaitForSeconds(3f);
        NPCmercaderTres.SetActive(false);
        NPCmercaderCuatro.SetActive(true);
        if (skip == false)
        {
            StartCoroutine(Texto5());
        }
    }
    IEnumerator Texto5()
    {
        yield return new WaitForSeconds(3f);
        NPCmercaderCuatro.SetActive(false);
        NPCmercaderCinco.SetActive(true);
        LinternaEnMano.SetActive(true);
        Linterna.SetActive(false);
        if (skip == false)
        {
            StartCoroutine(Texto6());
        }

    }
    IEnumerator Texto6()
    {
        yield return new WaitForSeconds(3f);
        NPCmercaderCinco.SetActive(false);
        NPCmercaderSeis.SetActive(true);
        if (skip == false)
        {
            StartCoroutine(Texto7());
        }

    }
    IEnumerator Texto7()
    {
        yield return new WaitForSeconds(3f);
        NPCmercaderSeis.SetActive(false);
        NPCmercaderSiete.SetActive(true);
       
    }

    IEnumerator Texto8()
    {
        yield return new WaitForSeconds(3f);
        NPCmercaderSiete.SetActive(false);
        Instrucciones1.SetActive(true);
        StartCoroutine(Finalizar());
    }


    IEnumerator Finalizar()
    {
        yield return new WaitForSeconds(3f);

        Instrucciones1.SetActive(false);

    }
}
