using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuertaMercader : MonoBehaviour
{
    public GameObject puertaCerrada;
    public GameObject puertaAbierta;
    public GameObject tocaE;
    void Start()
    {
        puertaAbierta.SetActive(false);
        puertaCerrada.SetActive(true);
        tocaE.SetActive(false);
    }

   
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            tocaE.SetActive(true);

        }
    }
    private void OnTriggerStay(Collider other)
    {
        if(Fusible.JugadorTieneFusible == true&& other.CompareTag("Player"))
        {
            tocaE.SetActive(false);
            puertaCerrada.SetActive(false);
            puertaAbierta.SetActive(true);
        }
    }
 
}
