using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycast : MonoBehaviour
{
    [SerializeField]
     private LayerMask whatToDetect;

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        float distance = 120f;
        Ray ray = new Ray(transform.position, transform.forward);
        Debug.DrawRay(ray.origin, ray.direction* distance, Color .green);
        
        if (Physics.Raycast(ray, out hit, distance, whatToDetect ))
        {
            GestorDeAudio.instancia.ReproducirSonido("LightEnemieGrito");
            hit.transform.gameObject.SetActive(false);
           
        }
    }
}
